package com.edgenda.bnc.skillsmanager.model;


import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;


@Entity
public class Invitation {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private Long eventId;

    @NotEmpty
    private String courriel;

    @NotEmpty
    private String confirmation;

    public Invitation() {
    }

    @PersistenceConstructor
    public Invitation(Long id, Long eventId , String courriel, String confirmation) {
        this.id = id;
        this.eventId = eventId;
        this.courriel = courriel;
        this.confirmation = confirmation;

    }

    @PersistenceConstructor
    public Invitation(Long eventId , String courriel) {
        this.eventId = eventId;
        this.courriel = courriel;
        this.confirmation = "pending";
    }

    public Long getId() {
        return id;
    } 

    public Long getEventId() {
        return eventId;
    }

    public String getCourriel() {
        return courriel;
    }
    public String getConfirmation() {
        return confirmation;
    }

}
