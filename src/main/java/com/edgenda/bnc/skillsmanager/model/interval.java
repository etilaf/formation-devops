package com.edgenda.bnc.skillsmanager.model;

public enum interval {
    NONE,
    DAY,
    WEEK,
    MONTH,
    YEAR;
}