package com.edgenda.bnc.skillsmanager.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UnknownInvitationException extends RuntimeException {

    public UnknownInvitationException(Long id) {
        super("Unknown Invitation with ID=" + id);
    }

}
