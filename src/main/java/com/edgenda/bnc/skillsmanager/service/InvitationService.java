package com.edgenda.bnc.skillsmanager.service;

import com.edgenda.bnc.skillsmanager.model.Invitation;
import com.edgenda.bnc.skillsmanager.repository.InvitationRepository;
import com.edgenda.bnc.skillsmanager.service.exception.UnknownInvitationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;

@Service
@Transactional

public class InvitationService {

    private final InvitationRepository invitationRepository;

    @Autowired
    public InvitationService(InvitationRepository invitationRepository) {
        this.invitationRepository = invitationRepository;
    }


    public Invitation createInvitation(Invitation invitation) {

        Assert.notNull(invitation, "Invitation cannot be null");
        final Invitation newInvitation = new Invitation(
                invitation.getEventId(),
                invitation.getCourriel()
        );
        return invitationRepository.save(newInvitation);
}

    public Invitation getInvitation(Long id){
        Assert.notNull(id, "Invitation ID cannot be null");
        return invitationRepository.findById(id)
                .orElseThrow(() -> new UnknownInvitationException(id));
    }

    public void updateInvitation(Long id, String confirmation) {
        Assert.notNull(id, "Invitation cannot be null");
        Invitation test = this.getInvitation(id);
        Invitation update = new Invitation(
                test.getId(),
                test.getEventId(),
                test.getCourriel(),
                confirmation
        );
        invitationRepository.save(update);
    }
}