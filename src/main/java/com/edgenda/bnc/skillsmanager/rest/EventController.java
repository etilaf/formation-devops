package com.edgenda.bnc.skillsmanager.rest;

import com.edgenda.bnc.skillsmanager.model.Event;
import com.edgenda.bnc.skillsmanager.service.EventService;
import com.edgenda.bnc.skillsmanager.model.Event;
import com.edgenda.bnc.skillsmanager.service.exception.AuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.jwt.JwtValidationException;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import com.edgenda.bnc.skillsmanager.model.Event;
import org.springframework.format.annotation.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.web.client.RestTemplate;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.http.client.ClientHttpRequestInterceptor;

import com.okta.jwt.Jwt;
import com.okta.jwt.JwtVerificationException;
import com.okta.jwt.AccessTokenVerifier;
import com.okta.jwt.JwtVerifiers;


@RestController
@RequestMapping(path = "/events")
public class EventController {

    private final OAuth2AuthorizedClientService authorizedClientService;
    private final RestTemplate restTemplate;
    private final EventService eventService;

    @Autowired
    public EventController(OAuth2AuthorizedClientService clientService,
                           RestTemplate restTemplate,
                           EventService eventService) {
        this.eventService = eventService;
        this.authorizedClientService = clientService;
        this.restTemplate = restTemplate;
    }

    private ClientHttpRequestInterceptor getBearerTokenInterceptor(String accessToken) {
        return (request, bytes, execution) -> {
            request.getHeaders().add("Authorization", "Bearer " + accessToken);
            return execution.execute(request, bytes);
        };
    }

    private void addBearerToHeader(JwtAuthenticationToken authentication) {
         restTemplate.getInterceptors().add(getBearerTokenInterceptor(authentication.getToken().getTokenValue()));
    }

    private String getEmail(JwtAuthenticationToken authentication) throws JwtVerificationException {
        AccessTokenVerifier jwtVerifier = JwtVerifiers.accessTokenVerifierBuilder()
                .setIssuer("https://dev-501349.okta.com/oauth2/default")
                .setAudience("api://default")      // defaults to 'api://default'
                .build();

        Jwt jwt = jwtVerifier.decode(authentication.getToken().getTokenValue());

        return jwt.getClaims().get("sub").toString();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Event createEvent(@RequestBody Event event) {
        return eventService.createEvent(event);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Event getEvent(@PathVariable Long id) {
        return eventService.getEvent(id);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public String updateEvent(@AuthenticationPrincipal JwtAuthenticationToken authentication, @PathVariable Long id, @RequestBody Event event) {
        try {
            addBearerToHeader(authentication);
            String email = getEmail(authentication);
            eventService.updateEvent(
                    new Event(
                            id,
                            event.getName(),
                            event.getStartDate(),
                            event.getEndDate(),
                            event.getStatus(),
                            event.getOwner(),
                            event.getDescription(),
                            event.getRecurring()
                    )
            );
            return "Updated";
        }
        catch (JwtVerificationException e) {
            throw new AuthenticationException();
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Event> listEventByOwner(
                                            @AuthenticationPrincipal JwtAuthenticationToken authentication,
                                            @RequestParam("owner") String owner,
                                            @RequestParam(name = "startPeriod",required = false, defaultValue = "1900-01-01T00:00:00") @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") Date startPeriod,
                                            @RequestParam(name = "endPeriod",required = false, defaultValue = "9999-12-31T23:59:59") @DateTimeFormat(pattern="yyyy-MM-dd'T'HH:mm:ss") Date endPeriod) {

        try {
            addBearerToHeader(authentication);
            String email = getEmail(authentication);
            return eventService.listEventByOwner(owner, startPeriod, endPeriod);
        }
        catch (JwtVerificationException e) {
            throw new AuthenticationException();
        }
    }


    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public String deleteEvent(@AuthenticationPrincipal JwtAuthenticationToken authentication, @PathVariable Long id) {
            try {
                addBearerToHeader(authentication);
                String email = getEmail(authentication);
                eventService.deleteEvent(id);
                return "L'evenement a été supprimé";
            }
            catch (JwtVerificationException e) {
                throw new AuthenticationException();
            }
    }


/*
    @RequestMapping(method = RequestMethod.GET)
    public Event getEvent(@RequestParam("owner") String owner) {
        return eventService.getEvent(owner);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Event getEmployee(@PathVariable Long id) {
        return employeeService.getEmployee(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Event getEvent(@RequestParam("owner") String owner) {
        return eventService.getEvent(owner);
    }
/*


    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Event createEmployee(@RequestBody Event event) {
        return employeeService.createEmployee(event);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public void updateEmployee(@PathVariable Long id, @RequestBody Event event) {
        employeeService.updateEmployee(
                new Event(
                        id,
                        event.getFirstName(),
                        event.getLastName(),
                        event.getEmail(),
                        event.getSkills()
                )
        );
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.deleteEmployee(id);
    }

    @RequestMapping(path = "/{id}/skills", method = RequestMethod.GET)
    public List<Skill> getEmployeeSkills(@PathVariable Long id) {
        return employeeService.getEmployeeSkills(id);
    }
*/
}
