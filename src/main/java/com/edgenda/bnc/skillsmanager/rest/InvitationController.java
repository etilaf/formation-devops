package com.edgenda.bnc.skillsmanager.rest;

import com.edgenda.bnc.skillsmanager.model.Event;
import com.edgenda.bnc.skillsmanager.model.Invitation;
import com.edgenda.bnc.skillsmanager.service.EventService;
import com.edgenda.bnc.skillsmanager.service.InvitationService;
import com.edgenda.bnc.skillsmanager.service.exception.AuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.web.client.RestTemplate;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.http.client.ClientHttpRequestInterceptor;

import com.okta.jwt.Jwt;
import com.okta.jwt.JwtVerificationException;
import com.okta.jwt.AccessTokenVerifier;
import com.okta.jwt.JwtVerifiers;

@RestController
@RequestMapping(path = "/invitation")
public class InvitationController {

    private final InvitationService invitationService;
    private final EventService eventService;
    private final OAuth2AuthorizedClientService authorizedClientService;
    private final RestTemplate restTemplate;

    @Autowired
    public InvitationController(OAuth2AuthorizedClientService clientService,
                                RestTemplate restTemplate,
                                InvitationService invitationService,
                                EventService eventService) {
        this.invitationService = invitationService;
        this.authorizedClientService = clientService;
        this.restTemplate = restTemplate;
        this.eventService = eventService;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Invitation getInvitation(@PathVariable Long id) {
        return invitationService.getInvitation(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Invitation createInvitation(@RequestBody Invitation invitation) {
        Invitation invite = invitationService.createInvitation(invitation);
        //Email email = new Email("skillsManager@gmail.com", "skillsManager", invitation.getCourriel(), "invité", "Vous êtes invité!",
        // "vous avez été invité à participer à l'événement #" + invitation.getEventId() + " veuillez confirmer votre présence dans les plus brefs délais. Merci et à bientôt!")
        //mailer.Send(email)

        Event event = eventService.getEvent(invite.getEventId());
        boolean succeeded = SendEmail.sendEmail("formation-Devops@formationDevops.com",
                                                invite.getCourriel(),
                                     "Vous avez reçu une invitation à un évènement",
                                "Vous êtes invité à l'évènement " + event.getName() + " qui aura lieu du " + event.getStartDate() + " au  " + event.getEndDate() + ". Veuillez confirmer votre présence à l'évènement. Merci et bonne journée !");
        if(succeeded)
        {
            return invite;
        }
        return invite;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public String updateInvitation(@AuthenticationPrincipal JwtAuthenticationToken authentication, @PathVariable Long id, @RequestBody String confirmation) {
        try {
            addBearerToHeader(authentication);
            String email = getEmail(authentication);
            invitationService.updateInvitation(id, confirmation);
            return "Updated";
        }
        catch (JwtVerificationException e) {
            throw new AuthenticationException();
        }
    }

    private String getEmail(JwtAuthenticationToken authentication) throws JwtVerificationException {
        AccessTokenVerifier jwtVerifier = JwtVerifiers.accessTokenVerifierBuilder()
                .setIssuer("https://dev-501349.okta.com/oauth2/default")
                .setAudience("api://default")      // defaults to 'api://default'
                .build();

        Jwt jwt = jwtVerifier.decode(authentication.getToken().getTokenValue());

        return jwt.getClaims().get("sub").toString();
    }

    private ClientHttpRequestInterceptor getBearerTokenInterceptor(String accessToken) {
        return (request, bytes, execution) -> {
            request.getHeaders().add("Authorization", "Bearer " + accessToken);
            return execution.execute(request, bytes);
        };
    }

    private void addBearerToHeader(JwtAuthenticationToken authentication) {
        restTemplate.getInterceptors().add(getBearerTokenInterceptor(authentication.getToken().getTokenValue()));
    }

    // public class SendEmail
    // {
   /* public static void main(String [] args){

        addBearerToHeader(authentication);
        String to = getEmail(authentication);
        String from = "pour_le_tests@gmail.com"; // a changer eventuellement

        //Date startDate = this.;
        //Date endDate   = this.;

        //Get the session object
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);
        Session session = Session.getDefaultInstance(properties);

        //compose the message
        try{
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  // Peut tre pas necessaire....
            message.setSubject("Vous avez reçu une invitation à un évènement");
            message.setText("Vous êtes invités à l'évènement "+ to + " qui aura lieu du " + to + "au " + to + "Veuillez confirmer votre présence à l'évènement. Merci et bonne journée ! ");
                    // Send message
                    Transport.send(message);
            System.out.println("Mail envoyé avec succés"); // juste pour confirmer l'Envoi ...

        }catch (MessagingException mex) {mex.printStackTrace();} catch (AddressException e) {
            e.printStackTrace();
        } catch (javax.mail.MessagingException e) {
            e.printStackTrace();
        }
    }
    //*/
}
