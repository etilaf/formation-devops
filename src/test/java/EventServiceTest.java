import com.edgenda.bnc.skillsmanager.model.Event;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class EventServiceTest {
    @Test
    public void testCreateEventWithEmpty(){
        Event newEventTest = new Event();
        assertNull(newEventTest.getId());
    }
}
