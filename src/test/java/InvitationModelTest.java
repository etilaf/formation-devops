import com.edgenda.bnc.skillsmanager.model.Invitation;
import org.junit.jupiter.api.Test;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class InvitationModelTest {

    @Test
    public void testCreateInvitationWithEmpty(){
        Invitation newInvitationTest = new Invitation();
        assertNull(newInvitationTest.getId());
    }

    @Test
    public void testCreateInvitationWithID(){
        Invitation newInvitationTest = new Invitation(1L,2L, "test@test.com", "pending");
        assertEquals("1", newInvitationTest.getId().toString());
        assertEquals("2", newInvitationTest.getEventId().toString());
        assertEquals("test@test.com", newInvitationTest.getCourriel());
        assertEquals("pending", newInvitationTest.getConfirmation());
    }

    @Test
    public void testCreateInvitationWithEmail(){
        Invitation newInvitationTest = new Invitation(2L, "test@test.com");
        assertEquals("2", newInvitationTest.getEventId().toString());
        assertEquals("test@test.com", newInvitationTest.getCourriel());
        assertEquals("pending", newInvitationTest.getConfirmation());
    }

}