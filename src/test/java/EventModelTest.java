import com.edgenda.bnc.skillsmanager.model.Event;
import com.edgenda.bnc.skillsmanager.model.interval;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class EventModelTest {
    @Test
    public void testCreateEventWithEmpty(){
        Event newEventTest = new Event();
        assertNull(newEventTest.getId());
    }
    @Test
    public void testCreateEventWithID(){
        Event newEventTest = new Event(1L,"test", new Date(2019,1,2),new Date(2019,2,3), "testStatus","alexis","testDescription", interval.NONE);
        assertEquals("1", newEventTest.getId().toString());
        assertEquals("test", newEventTest.getName());
        assertEquals(new Date(2019,1,2), newEventTest.getStartDate());
        assertEquals(new Date(2019,2,3), newEventTest.getEndDate());
        assertEquals("testStatus", newEventTest.getStatus());
        assertEquals("alexis", newEventTest.getOwner());
        assertEquals("testDescription", newEventTest.getDescription());
    }

    @Test
    public void testCreateEventWithName(){
        Event newEventTest = new Event("test", new Date(),new Date(), "testStatus","alexis","testDescription", interval.NONE);
        assertEquals("test", newEventTest.getName());
    }
}
