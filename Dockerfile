FROM openjdk:8
COPY ./target/skills-manager-0.0.1-SNAPSHOT.jar /usr/src/app/
WORKDIR /usr/src/app/
EXPOSE 8091
CMD java -jar skills-manager-0.0.1-SNAPSHOT.jar
